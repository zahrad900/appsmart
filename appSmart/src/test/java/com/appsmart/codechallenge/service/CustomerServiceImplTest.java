package com.appsmart.codechallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

	@Mock
	private CustomerRepository customerRepository;

	@InjectMocks
	private CustomerServiceImpl customerService;

	private Customer customer1;
	private Customer customer2;
	private Customer customer3;
	private Customer customer4;
	private List<Customer> customerList;
	private Page<Customer> customerPage;
	private Pageable pageable;

	@BeforeEach
	public void setUp() {
		customerList = new ArrayList<>();

		customer1 = new Customer(UUID.randomUUID(), null, "customer 1", false, LocalDateTime.now(), null);
		customer2 = new Customer(UUID.randomUUID(), null, "customer 2", false, LocalDateTime.now(), null);
		customer3 = new Customer(UUID.randomUUID(), null, "customer 3", false, LocalDateTime.now(), null);
		customer4 = new Customer(UUID.randomUUID(), null, "customer 4", false, LocalDateTime.now(), null);

		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);
		customerList.add(customer4);

		pageable = PageRequest.of(0, 2);
		customerPage = new PageImpl<Customer>(customerList.subList(0, 2), pageable, 4);
	}

	@Test
	public void GivenFindAllCustomerShouldReturnListOfAllCustomer() {

		when(customerRepository.findAll()).thenReturn(customerList);
		List<Customer> customerList1 = customerService.findAll();
		assertEquals(customerList1, customerList);
		verify(customerRepository, times(1)).findAll();
	}

	@Test
	void givenCustomerToAddShouldReturnAddedCustomer() {

		when(customerRepository.save(any())).thenReturn(customer1);
		Customer customer = customerService.save(customer1);
		assertEquals(customer, customer1);
		verify(customerRepository, times(1)).save(any());
	}

	@Test
	void givenCustomerToUpdateShouldReturnUpdateedCustomer() throws ResourceNotFoundException {

		when(customerRepository.save(any())).thenReturn(customer4);
		when(customerRepository.findById(customer4.getId())).thenReturn(Optional.ofNullable(customer4));
		Customer customer = customerService.update(customer4.getId(), customer4);
		assertEquals(customer, customer4);
		verify(customerRepository, times(1)).save(any());
	}

	@Test
	void givenNotExistCustomerIdToFindShouldThrowException() throws ResourceNotFoundException {

		when(customerRepository.findById(customer4.getId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			customerService.findById(customer4.getId());
		});

	}

	@Test
	void givenNotExistCustomerIdToUpdateShouldThrowException() throws ResourceNotFoundException {
		when(customerRepository.findById(customer4.getId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			customerService.update(customer4.getId(), customer4);
		});
		verify(customerRepository, never()).save(any(Customer.class));
	}

	@Test
	public void givenIdThenShouldReturnCustomerOfThatId() throws ResourceNotFoundException {
		when(customerRepository.findById(customer1.getId())).thenReturn(Optional.ofNullable(customer1));
		assertEquals(customerService.findById(customer1.getId()), customer1);
	}

	@Test
	public void GivenFindAllPageableCustomerShouldReturnFirstPageOfListOfCustomer() {

		when(customerRepository.findAll(pageable)).thenReturn(customerPage);
		Page<Customer> customerPage1 = customerService.findAll(pageable);
		assertEquals(customerPage1, customerPage);
		verify(customerRepository, times(1)).findAll(pageable);
	}

	@Test
	public void givenIdTODeleteLogicalThenShouldReturnDeletedCustomer() throws Exception {
		when(customerRepository.findById(customer1.getId())).thenReturn(Optional.ofNullable(customer1));
		when(customerRepository.save(customer1)).thenReturn(customer1);

		Customer customer = customerService.deleteByIdLogical(customer1.getId());

		assertEquals(customer.getIsDeleted(), true);
		verify(customerRepository, times(1)).save(any());
	}
}
