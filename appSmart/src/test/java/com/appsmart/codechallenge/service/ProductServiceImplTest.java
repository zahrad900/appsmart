package com.appsmart.codechallenge.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.model.Product;
import com.appsmart.codechallenge.repository.ProductRepository;

@ExtendWith(MockitoExtension.class)
public class ProductServiceImplTest {

	@Mock
	private ProductRepository productRepository;

	@Mock
	CustomerService customerService;

	@InjectMocks
	private ProductServiceImpl productService;

	private Product product1;
	private Product product2;
	private Product product3;
	private Product product4;
	private List<Product> productList;
	private Page<Product> productPage;
	private Pageable pageable;

	private Customer customer1;
	private Customer customer2;

	@BeforeEach
	public void setUp() {

		customer1 = new Customer(UUID.randomUUID(), null, "customer 1", false, LocalDateTime.now(), null);
		customer2 = new Customer(UUID.randomUUID(), null, "customer 2", false, LocalDateTime.now(), null);

		productList = new ArrayList<>();

		product1 = new Product(UUID.randomUUID(), customer1, "product 1", "the first product", BigDecimal.valueOf(10),
				false, LocalDateTime.now(), null);
		product2 = new Product(UUID.randomUUID(), customer1, "product 2", "the second product",
				BigDecimal.valueOf(14.99), false, LocalDateTime.now(), null);
		product3 = new Product(UUID.randomUUID(), customer2, "product 3", "the third product",
				BigDecimal.valueOf(45.00), false, LocalDateTime.now(), null);
		product4 = new Product(UUID.randomUUID(), customer2, "product 4", "the fourth product",
				BigDecimal.valueOf(9.99), false, LocalDateTime.now(), null);

		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		productList.add(product4);

		pageable = PageRequest.of(0, 2);
		productPage = new PageImpl<Product>(productList.subList(0, 2), pageable, 4);

	}

	@Test
	public void GivenFindProductByCustomerShouldReturnListOfAllProduct() throws ResourceNotFoundException {

		when(productRepository.findByCustomer(customer1, pageable)).thenReturn(productPage);
		when(customerService.findById(customer1.getId())).thenReturn(customer1);
		Page<Product> productPage1 = productService.findByCustomerId(customer1.getId(), pageable);
		assertEquals(productPage1, productPage);
		verify(productRepository, times(1)).findByCustomer(customer1, pageable);
	}

	@Test
	void givenProductToAddShouldReturnAddedProduct() throws Exception {

		when(productRepository.save(any())).thenReturn(product1);
		Product product = productService.save(product1);
		assertEquals(product, product1);
		verify(productRepository, times(1)).save(any());
	}

	@Test
	void givenProductToUpdateShouldReturnUpdateedProduct() throws ResourceNotFoundException {

		when(productRepository.save(any())).thenReturn(product4);
		when(productRepository.findById(product4.getId())).thenReturn(Optional.ofNullable(product4));
		Product product = productService.update(product4.getId(), product4);
		assertEquals(product, product4);
		verify(productRepository, times(1)).save(any());
	}

	@Test
	void givenNotExistProductIdToFindShouldThrowException() throws ResourceNotFoundException {

		when(productRepository.findById(product4.getId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			productService.findById(product4.getId());
		});

	}

	@Test
	void givenNotExistProductIdToUpdateShouldThrowException() throws ResourceNotFoundException {
		when(productRepository.findById(product4.getId())).thenReturn(Optional.empty());
		assertThrows(ResourceNotFoundException.class, () -> {
			productService.update(product4.getId(), product4);
		});
		verify(productRepository, never()).save(any(Product.class));
	}

	@Test
	public void givenIdThenShouldReturnProductOfThatId() throws ResourceNotFoundException {
		when(productRepository.findById(product1.getId())).thenReturn(Optional.ofNullable(product1));
		assertEquals(productService.findById(product1.getId()), product1);
	}

	@Test
	public void givenIdTODeleteLogicalThenShouldReturnDeletedProduct() throws Exception {
		when(productRepository.findById(product1.getId())).thenReturn(Optional.ofNullable(product1));
		when(productRepository.save(product1)).thenReturn(product1);

		Product product = productService.deleteByIdLogical(product1.getId());

		assertEquals(product.getIsDeleted(), true);
		verify(productRepository, times(1)).save(any());
	}
}
