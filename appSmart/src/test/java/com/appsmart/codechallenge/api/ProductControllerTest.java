package com.appsmart.codechallenge.api;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.model.Product;
import com.appsmart.codechallenge.service.CustomerService;
import com.appsmart.codechallenge.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = ProductController.class)
public class ProductControllerTest {
	public static final String PRODUCT_API_BASE_URL = "/api/v1";

	@MockBean
	private ProductService productService;

	@MockBean
	private CustomerService customerService;

	private Product product1;
	private Product product2;
	private Product product3;
	private Product product4;
	private List<Product> productList;
	private Page<Product> productPage;
	private Pageable pageable;

	private Customer customer1;
	private Customer customer2;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeEach
	public void setup() {
		customer1 = new Customer(UUID.randomUUID(), null, "customer 1", false, LocalDateTime.now(), null);
		customer2 = new Customer(UUID.randomUUID(), null, "customer 2", false, LocalDateTime.now(), null);

		productList = new ArrayList<>();

		product1 = new Product(UUID.randomUUID(), customer1, "product 1", "the first product", BigDecimal.valueOf(10),
				false, LocalDateTime.now(), null);
		product2 = new Product(UUID.randomUUID(), customer1, "product 2", "the second product",
				BigDecimal.valueOf(14.99), false, LocalDateTime.now(), null);
		product3 = new Product(UUID.randomUUID(), customer2, "product 3", "the third product",
				BigDecimal.valueOf(45.00), false, LocalDateTime.now(), null);
		product4 = new Product(UUID.randomUUID(), customer2, "product 4", "the fourth product",
				BigDecimal.valueOf(9.99), true, LocalDateTime.now(), null);

		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		productList.add(product4);

		pageable = PageRequest.of(0, 2);
		productPage = new PageImpl<Product>(productList.subList(0, 2), pageable, 4);

	}

	@AfterEach
	void tearDown() {
		product1 = product2 = product3 = product4 = null;
	}

	@Test
	void PostMappingOfProduct() throws Exception {

		when(productService.save(any())).thenReturn(product1);
		mockMvc.perform(post(PRODUCT_API_BASE_URL + "/customers/{customerId}/products", customer1.getId())
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(product1)))
				.andExpect(status().isCreated());

		verify(productService, times(1)).save(any());
	}

	@Test
	void GetMappingOfAllProductOfCustomer() throws Exception {
		given(productService.findByCustomerId(customer1.getId(), PageRequest.of(0, 2))).willReturn(productPage);
		mockMvc.perform(get(PRODUCT_API_BASE_URL + "/customers/{customerId}/products", customer1.getId())
				.param("page", "0").param("size", "2").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(pageable))).andExpect(status().isOk())
				.andExpect(jsonPath("$.content[0].title", is(product1.getTitle())))
				.andExpect(jsonPath("$.content[1].title", is(product2.getTitle())))
				.andExpect(jsonPath("$.pageable.pageNumber", is(0))).andExpect(jsonPath("$.pageable.pageSize", is(2)));
	}

	@Test
	void GetMappingOfProductShouldReturnRespectiveProduct() throws Exception {

		given(productService.findById(product1.getId())).willReturn(product1);

		mockMvc.perform(get(PRODUCT_API_BASE_URL + "/products/{id}", product1.getId())).andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is(product1.getTitle())));
	}

	@Test
	void DeleteMappingUrlAndIdThenShouldReturnDeletedProduct() throws Exception {
		given(productService.deleteByIdLogical(product4.getId())).willReturn(product4);
		mockMvc.perform(delete(PRODUCT_API_BASE_URL + "/products/{id}", product4.getId())
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(product4)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.title", is(product4.getTitle())))
				.andExpect(jsonPath("$.isDeleted", is(true)));
	}

	@Test
	void GetMappingOfNonExistProductIdShouldReturnR400() throws Exception {

		UUID randomID = UUID.randomUUID();
		given(productService.findById(randomID)).willThrow(ResourceNotFoundException.class);

		mockMvc.perform(get(PRODUCT_API_BASE_URL + "/products/{id}", randomID)).andExpect(status().isNotFound());
	}

	@Test
	void PutMappingOfProductShouldReturnUpdatedProduct() throws Exception {

		given(productService.update(any(), any())).willAnswer((invocation) -> invocation.getArgument(1));

		mockMvc.perform(put(PRODUCT_API_BASE_URL + "/products/{id}", product3.getId())
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(product3)))
				.andExpect(status().isOk());

	}
}
