package com.appsmart.codechallenge.api;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(controllers = CustomerController.class)
public class CustomerControllerTest {

	public static final String CUSTOMER_API_BASE_URL = "/api/v1/customers";

	@MockBean
	private CustomerService customerService;

	private Customer customer1;
	private Customer customer2;
	private Customer customer3;
	private Customer customer4;
	private List<Customer> customerList;
	private Page<Customer> customerPage;
	private Pageable pageable;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@BeforeEach
	public void setup() {
		customerList = new ArrayList<>();

		customer1 = new Customer(UUID.randomUUID(), null, "customer 1", false, LocalDateTime.now(), null);
		customer2 = new Customer(UUID.randomUUID(), null, "customer 2", false, LocalDateTime.now(), null);
		customer3 = new Customer(UUID.randomUUID(), null, "customer 3", false, LocalDateTime.now(), null);
		customer4 = new Customer(UUID.randomUUID(), null, "customer 4", true, LocalDateTime.now(), null);

		customerList.add(customer1);
		customerList.add(customer2);
		customerList.add(customer3);
		customerList.add(customer4);

		pageable = PageRequest.of(0, 2);
		customerPage = new PageImpl<Customer>(customerList.subList(0, 2), pageable, 4);

	}

	@AfterEach
	void tearDown() {
		customer1 = customer2 = customer3 = customer4 = null;
	}

	@Test
	void PostMappingOfCustomer() throws Exception {

		when(customerService.save(any())).thenReturn(customer1);
		mockMvc.perform(post(CUSTOMER_API_BASE_URL).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer1))).andExpect(status().isCreated());

		verify(customerService, times(1)).save(any());
	}

	@Test
	void GetMappingOfAllCustomer() throws Exception {
		given(customerService.findAll(PageRequest.of(0, 2))).willReturn(customerPage);
		mockMvc.perform(get(CUSTOMER_API_BASE_URL).param("page", "0").param("size", "2")
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(pageable)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.content[0].title", is(customer1.getTitle())))
				.andExpect(jsonPath("$.content[1].title", is(customer2.getTitle())))
				.andExpect(jsonPath("$.pageable.pageNumber", is(0))).andExpect(jsonPath("$.pageable.pageSize", is(2)));
	}

	@Test
	void GetMappingOfCustomerShouldReturnRespectiveCustomer() throws Exception {

		given(customerService.findById(customer1.getId())).willReturn(customer1);

		mockMvc.perform(get(CUSTOMER_API_BASE_URL + "/{id}", customer1.getId())).andExpect(status().isOk())
				.andExpect(jsonPath("$.title", is(customer1.getTitle())));
	}

	@Test
	void DeleteMappingUrlAndIdThenShouldReturnDeletedCustomer() throws Exception {
		given(customerService.deleteByIdLogical(customer4.getId())).willReturn(customer4);
		mockMvc.perform(delete(CUSTOMER_API_BASE_URL + "/{id}", customer4.getId())
				.contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(customer4)))
				.andExpect(status().isOk()).andExpect(jsonPath("$.title", is(customer4.getTitle())))
				.andExpect(jsonPath("$.isDeleted", is(true)));
	}

	@Test
	void GetMappingOfNonExistCustomerIdShouldReturnR400() throws Exception {

		UUID randomID = UUID.randomUUID();
		given(customerService.findById(randomID)).willThrow(ResourceNotFoundException.class);

		mockMvc.perform(get(CUSTOMER_API_BASE_URL + "/{id}", randomID)).andExpect(status().isNotFound());
	}

	@Test
	void PutMappingOfCustomerShouldReturnUpdatedCustomer() throws Exception {

		given(customerService.update(any(), any())).willAnswer((invocation) -> invocation.getArgument(1));

		mockMvc.perform(put(CUSTOMER_API_BASE_URL + "/{id}", customer3.getId()).contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(customer3))).andExpect(status().isOk());

	}

}
