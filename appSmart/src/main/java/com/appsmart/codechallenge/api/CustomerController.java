package com.appsmart.codechallenge.api;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.service.CustomerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/customers")
public class CustomerController {

	private final CustomerService customerService;
	private static final int DEFULT_PAGE_SIZE = 10;

	@GetMapping
	public ResponseEntity<Page<Customer>> getAllCustomers(@PageableDefault(size = DEFULT_PAGE_SIZE) Pageable pageable) {
		Page<Customer> customers = customerService.findAll(pageable);
		if (customers.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(customers, HttpStatus.OK);

	}

	@PostMapping
	public ResponseEntity<Customer> createCustomer(@Valid @RequestBody Customer customer) {

		return new ResponseEntity<>(customerService.save(customer), HttpStatus.CREATED);

	}

	// logical delete
	@DeleteMapping(value = "/{customerId}")
	public ResponseEntity<Customer> deleteCustomer(@PathVariable(value = "customerId") UUID customerId)
			throws Exception {
		return new ResponseEntity<>(customerService.deleteByIdLogical(customerId), HttpStatus.OK);
	}

	@GetMapping(value = "/{customerId}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable(value = "customerId") UUID customerId)
			throws ResourceNotFoundException {

		return new ResponseEntity<>(customerService.findById(customerId), HttpStatus.OK);
	}

	@PutMapping(value = "/{customerId}")
	public ResponseEntity<Customer> updateCustomer(@PathVariable(value = "customerId") UUID customerId,
			@RequestBody Customer customer) throws ResourceNotFoundException {

		return new ResponseEntity<>(customerService.update(customerId, customer), HttpStatus.OK);

	}

}
