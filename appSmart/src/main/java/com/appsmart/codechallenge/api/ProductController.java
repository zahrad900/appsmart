package com.appsmart.codechallenge.api;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.model.Product;
import com.appsmart.codechallenge.service.CustomerService;
import com.appsmart.codechallenge.service.ProductService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class ProductController {
	private static final int DEFULT_PAGE_SIZE = 10;

	private final ProductService productService;
	private final CustomerService customerService;

	@GetMapping("/customers/{customerId}/products")
	public ResponseEntity<Page<Product>> getProductsByCustomerId(@PathVariable(value = "customerId") UUID customerId,
			@PageableDefault(size = DEFULT_PAGE_SIZE) Pageable pageable) throws ResourceNotFoundException {
		Page<Product> products = productService.findByCustomerId(customerId, pageable);

		if (products.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@PostMapping("/customers/{customerId}/products")
	public ResponseEntity<Product> createProductForCustomer(@PathVariable(value = "customerId") UUID customerId,
			@RequestBody Product product) throws Exception {

		Customer customer = customerService.findById(customerId);
		product.setCustomer(customer);

		return new ResponseEntity<>(productService.save(product), HttpStatus.CREATED);

	}

	//logical delete
	@DeleteMapping(value = "/products/{productId}")
	public ResponseEntity<Product> deleteProduct(@PathVariable(value = "productId") UUID productId) throws Exception {
		return new ResponseEntity<>(productService.deleteByIdLogical(productId), HttpStatus.OK);
	}

	@GetMapping(value = "/products/{productId}")
	public ResponseEntity<Product> getProductById(@PathVariable(value = "productId") UUID productId)
			throws ResourceNotFoundException {

		return new ResponseEntity<>(productService.findById(productId), HttpStatus.OK);

	}

	@PutMapping(value = "/products/{productId}")
	public ResponseEntity<Product> updateProduct(@PathVariable(value = "productId") UUID productId,
			@RequestBody Product product) throws ResourceNotFoundException {

		return new ResponseEntity<>(productService.update(productId, product), HttpStatus.OK);

	}

}
