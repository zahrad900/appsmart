package com.appsmart.codechallenge.exception;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorDetails {
	private LocalDate date;
	private String message;
	private String details;
}
