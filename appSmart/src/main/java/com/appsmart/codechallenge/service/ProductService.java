package com.appsmart.codechallenge.service;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Product;

public interface ProductService {

	Page<Product> findByCustomerId(UUID customerId, Pageable pageable) throws ResourceNotFoundException;

	Product findById(UUID id) throws ResourceNotFoundException;

	Product save(Product product) throws Exception;

	Product update(UUID productId, Product product) throws ResourceNotFoundException;

	void deleteById(UUID id);

	Product deleteByIdLogical(UUID id) throws Exception;

}
