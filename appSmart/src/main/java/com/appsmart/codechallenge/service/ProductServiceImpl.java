package com.appsmart.codechallenge.service;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.model.Product;
import com.appsmart.codechallenge.repository.ProductRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

	private final ProductRepository productRepository;
	private final CustomerService customerService;

	@Override
	public Page<Product> findByCustomerId(UUID customerId, Pageable pageable) throws ResourceNotFoundException {
		Customer customer = customerService.findById(customerId);
		return productRepository.findByCustomer(customer, pageable);
	}

	@Override
	public Product findById(UUID id) throws ResourceNotFoundException {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Product is not found for this id :: " + id));
		return product;
	}

	@Override
	public Product save(Product product) throws Exception {

		return productRepository.save(product);
	}

	@Override
	public void deleteById(UUID productId) {
		productRepository.deleteById(productId);

	}

	@Override
	public Product deleteByIdLogical(UUID id) throws Exception {
		Product product = productRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Product is not found for this id :: " + id));

		product.setIsDeleted(true);
		return productRepository.save(product);
	}

	@Override
	public Product update(UUID productId, Product product) throws ResourceNotFoundException {
		Product _product = productRepository.findById(productId)
				.orElseThrow(() -> new ResourceNotFoundException("Product is not found for this id :: " + productId));

		if (product.getCustomer() != null) {
			Customer _customer = customerService.findById(product.getCustomer().getId());
			_product.setCustomer(_customer);
		}

		_product.setTitle(product.getTitle());
		_product.setPrice(product.getPrice());
		if (product.getDescription() != null)
			_product.setDescription(product.getDescription());

		return productRepository.save(_product);
	}

}
