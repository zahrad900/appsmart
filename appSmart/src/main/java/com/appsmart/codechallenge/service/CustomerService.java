package com.appsmart.codechallenge.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;

public interface CustomerService {

	List<Customer> findAll();

	Customer findById(UUID id) throws ResourceNotFoundException;

	Customer save(Customer customer);

	void deleteById(UUID id);

	Customer deleteByIdLogical(UUID id) throws Exception;

	Customer update(UUID customerId, Customer customer) throws ResourceNotFoundException;

	Page<Customer> findAll(Pageable pageable);

}
