package com.appsmart.codechallenge.service;

import java.util.List;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.appsmart.codechallenge.exception.ResourceNotFoundException;
import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.repository.CustomerRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

	private final CustomerRepository customerRepository;

	@Override
	public List<Customer> findAll() {
		return customerRepository.findAll();
	}

	@Override
	public Customer findById(UUID id) throws ResourceNotFoundException {
		Customer customer = customerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer is not found for this id :: " + id));
		return customer;
	}

	@Override
	public Customer save(Customer customer) {
		return customerRepository.save(customer);
	}

	@Override
	public void deleteById(UUID id) {
		customerRepository.deleteById(id);
		;
	}

	@Override
	public Customer deleteByIdLogical(UUID id) throws Exception {
		Customer customer = customerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer is not found for this id :: " + id));

		customer.setIsDeleted(true);
		return customerRepository.save(customer);
	}

	@Override
	public Customer update(UUID customerId, Customer customer) throws ResourceNotFoundException {
		Customer _customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new ResourceNotFoundException("Customer is not found for this id :: " + customerId));

		_customer.setTitle(customer.getTitle());

		return customerRepository.save(_customer);
	}

	@Override
	public Page<Customer> findAll(Pageable pageable) {

		return customerRepository.findAll(pageable);
	}

}
