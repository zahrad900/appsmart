package com.appsmart.codechallenge.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.appsmart.codechallenge.model.Customer;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {

}
