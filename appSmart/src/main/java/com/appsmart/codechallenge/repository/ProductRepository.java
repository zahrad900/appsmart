package com.appsmart.codechallenge.repository;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.appsmart.codechallenge.model.Customer;
import com.appsmart.codechallenge.model.Product;

public interface ProductRepository extends JpaRepository<Product, UUID> {

	Page<Product> findByCustomer(Customer customer, Pageable pageable);

}
